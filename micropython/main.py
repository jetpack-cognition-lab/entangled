import machine
import network
import utime
import socket
import secrets
import time

"""
    # copy file to pico:
    ampy -p /dev/ttyACM0 put main.py

    #run main file:
    ampy -p /dev/ttyACM0 run main.py

    #list files:
    ampy -p /dev/ttyACM0 ls

    #connect via terminal:
    minicom -o -D /dev/ttyACM0

"""

# Turn on the Raspberry Pi Pico W’s Wi-Fi.
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
led = machine.Pin("LED", machine.Pin.OUT)

data = "hello pi"
hostname = "78.47.55.149"
port = 7331

# things to do in a loop
def loop():
    led.on()

    # Connect to router using SSID and PASSWORD stored in the secrets.py
    if not wlan.isconnected():
        wlan.connect(secrets.SSID, secrets.PASSWORD)

        timeout = 10
        while timeout > 0:
            if wlan.status() < 0 or wlan.status() >= 3:
                break
            timeout -= 1
            print('Waiting for connection...')
            time.sleep(1)

        if wlan.status() != 3:
            led.off()
            raise RuntimeError('wifi connection failed')# TODO: fix
        else:
            print('connected')
            status = wlan.ifconfig()
            print( 'ip = ' + status[0] )

    addr_info = socket.getaddrinfo(hostname, port)
    addr = addr_info[0][-1]

    s = socket.socket()
    s.connect(addr)

    i = 0
    avg = 0.0
    while True:
        led.toggle()
        i = i + 1
        sdat = data+str(i)
        start = time.ticks_ms()
        s.send(sdat)
        r = s.recv(32)
        end = time.ticks_ms()
        dt = end - start
        avg = 0.05*dt+0.95*avg
        print("recv: {0} dt: {1} ms ({2})".format(r, dt, round(avg)))
    #    utime.sleep(0.1)

def main():
    try:
        loop()
    except socket.error as e:
        print(str(e))


if __name__ == "__main__": main()
