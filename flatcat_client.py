#!/usr/bin/python

import socket
import sys
import argparse
import time
# Create a connection to the server application on port 81

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--data' , default='helloworld')
    parser.add_argument('-n', '--hostname', default='localhost')
    args = parser.parse_args()


    tcp_socket = socket.create_connection((args.hostname, 7331))

    try:
        data = args.data

        while(1):
            start = time.time()
            tcp_socket.sendall(data)
            foo = tcp_socket.recv(1024)
            end = time.time()
            dt = end - start
            if foo == 'END':
                print("server disconnected")
                break
            print("loopback: {0} dt: {1:4.1f} ms".format(foo, round(dt*10000)/10))
            time.sleep(0.2)

    except socket.error as e:
        print(str(e))

    except KeyboardInterrupt:
        print("Aborted.")

    finally:
        print("Closing socket")
        tcp_socket.close()

if __name__ == "__main__": main()
