#!/usr/bin/python

import os, sys
import argparse
import socket
import threading
import time


debug = False

def dbg_msg(mgs):
    if debug:
        print("\033[95m> {}\033[0m".format(mgs))



logo = r"""
                                            .            .
    flatcat       /                        /            /
     .-.   .-.---/---.-.   .-.    .-.     /   .-.  .-../
   ./.-'  /   ) /   (  |  /   )  (   )   /  ./.-'_(   /
   (__.''/   ( /     `-'-'   (    `-/-'_/_.-(__.'  `-'-..
              `-              `--._/              server
"""


class TCPclient:
    def __init__(self, id, client, flag):
        self.data = []
        self.id = id
        self.done = False

        dbg_msg('[{0}] starting'.format(self.id))
        self.thread = threading.Thread(target=self.handle_connection, args=(client, flag))
        self.thread.start()

    def handle_connection(self, connection, flag):
        while flag.is_set():
            data = connection.recv(2048) # echo data
            dbg_msg("[{0}] <{1}>".format(self.id, data))
            response = 'R:' + data
            if not data:
                break
            connection.sendall(response)
        dbg_msg("[{0}] closing".format(self.id))
        data = connection.recv(2048)
        connection.sendall('END')
        connection.close()
        self.done = True
        print("[{0}] disconnected".format(self.id))

    def join(self):
        if self.thread.is_alive():
            self.thread.join()
        dbg_msg("[{0}] ended".format(self.id))


def cleanup(client_list):
    for c in client_list:
        if c.done:
            c.join()
            client_list.remove(c)


def main():
    global debug
    
    print("\033[93m"+logo+"\033[0m")

    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--hostname' , default='') # '' is preferred over '127.0.0.1'
    parser.add_argument('-p', '--port'     , default=7331)
    parser.add_argument('-l', '--limit'    , default=256)
    parser.add_argument('-x', '--debug'    , action='store_true')

    args = parser.parse_args()
    debug = args.debug

    server_address = (args.hostname, int(args.port))
    print("address = {}".format(server_address))

    max_connections = int(args.limit)
    print("max. connections = {}".format(max_connections))

    dbg_msg("creating socket")
    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    dbg_msg("binding server address")
    try:
        tcp_socket.bind(server_address)
    except socket.error as e:
        print(str(e))
        sys.exit(1)

    dbg_msg('socket is listening')
    tcp_socket.listen(5)

    flag = threading.Event()
    flag.set()
    next_id = 0 # unique id for each client
    client_list = []

    try:
        while True:
            # spawning new connection threads
            client, address = tcp_socket.accept()
            print('client {0} connected to {1}:{2}'.format(next_id, address[0], address[1]))
            client_list.append(TCPclient(next_id, client, flag))
            next_id += 1
            while True:
                cleanup(client_list)
                if len(client_list) < max_connections:
                    break
                # max. clients reached, waiting...
                time.sleep(1)
            print("number of clients: {}/{}".format(len(client_list),max_connections))

    except KeyboardInterrupt:
        print("aborted by user")
        flag.clear()

    finally:
        dbg_msg("waiting for threads to close")
        cleanup(client_list)

        dbg_msg("closing socket")
        tcp_socket.close()

    print("____\nDONE.")


if __name__ == "__main__": main()
