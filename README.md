# entangled_srv

start server: 
```
./entangled_server.py
```	
	
start client(s): 
```
# -n [hostname]
# -d [teststring]
./flatcat_client.py -n localhost -d helloworld
```	
	
start client on pi pico:
1. install current micropython by copying the *.uf2 file to the USB drive after connecting
2. install the client:

```	
# copy files to pico:
ampy -p /dev/ttyACM0 put main.py
ampy -p /dev/ttyACM0 put secrets.py

#run main file:	
ampy -p /dev/ttyACM0 run main.py
```
